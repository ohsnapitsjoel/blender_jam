# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "BlenderJam!",
    "author": "Joel Daniels",
    "version": (0, 3),
    "blender": (2, 7, 2),
    "location": "Properties > Scene",
    "description": "Export OpenGL renders on keyframes to MonkeyJam and update the changes in Blender",
    "wiki_url": "https://bitbucket.org/ohsnapitsjoel/blender_jam/wiki/Home",
    "tracker_url": "https://bitbucket.org/ohsnapitsjoel/blender_jam/issues",
    "category": "Animation"
}


import bpy
from subprocess import Popen
import os, sys
from extensions_framework import util as efutil

imageNames = []

sep = os.path.sep
#---------------------------------------------------------
def realpath(path):
    '''Get the absolute path to the directory'''
    return os.path.abspath(efutil.filesystem_path(path))
#---------------------------------------------------------
def remember_frames(context):
    '''Get the frames we'll be rendering'''
    scene = context.scene
    object = context.active_object
    start_frame = scene.frame_start
    end_frame = scene.frame_end
    mj_keyframes = set()
    
    for keyframe in range(start_frame, end_frame + 1):
        if object.animation_data is not None:
            action = object.animation_data.action
            if action is not None:
                for fcu in action.fcurves:
                    if keyframe in (p.co.x for p in fcu.keyframe_points):
                        mj_keyframes.add(int(keyframe))
    return sorted(mj_keyframes)   #sort the set numerically, so the frames render in order  
#---------------------------------------------------------
def get_audio(self, scene):
    '''Get the audio file to be exported, if any'''
    if scene.monkeyjam.export_audio:
        if scene.monkeyjam.audio_sequ:
            return realpath(bpy.data.sounds[scene.monkeyjam.audio_sequ].filepath)
        else:
            self.report({'INFO'}, "Audio export is enabled, but no audio file is selected!")
        
        
    
#------------------------------------------------------------------------------
#----------------------------------------------------#
#                                                    #
# A class for exporting the keyframes to MonkeyJam   #
#                                                    #
#----------------------------------------------------#

class PROPERTIES_OT_Export(bpy.types.Operator):
    """Export frames to Monkey Jam .xps file!"""
    bl_idname = "mj.export_frames"
    bl_label = "MonkeyJam Export"
    
    def _create_image_names(self, scene, frame_list):
        '''Render out the viewport images'''

        #Directory and file name for rendering
        output_path = realpath(scene.monkeyjam.export_path)   
        filepath_place_holder = scene.render.filepath
        size_placeholder = scene.render.resolution_percentage
        global imageNames
        imageNames = []
        
        # Need to normalize the frames so that the first image written to the .xps file is 
        #   written to frame 1, regardless of its actual position on the timeline in Blender
        offset = 0
        if frame_list[0] != 1:
            offset = 1 - frame_list[0]
        
        if scene.monkeyjam.render_globals == '25':
            scene.render.resolution_percentage = 25
        elif scene.monkeyjam.render_globals == '50':
            scene.render.resolution_percentage = 50
        elif scene.monkeyjam.render_globals == '75':
            scene.render.resolution_percentage = 75
        else:
            scene.render.resolution_percentage = 100

        include_rigs = scene.monkeyjam.include_rigs
        
        try:
            for frame in frame_list:
                scene.frame_set(frame)
                scene.render.filepath = os.path.join(output_path, scene.name) + str(frame)
                bpy.ops.render.opengl(write_still = True, view_context = include_rigs)
                imageNames.append((scene.name + str(frame), frame + offset))        
            scene.render.filepath = filepath_place_holder    #Reset to the user's filepath
            scene.render.resolution_percentage = size_placeholder
            
        except TypeError:
            self.report( {'INFO'}, "Seriously. The .xps file will be empty. Select an armature and try again.")    

    #----------------------------------------------------#
    # Functions to write out xml with img names          #
    #                                                    #
    #----------------------------------------------------#
    
    def _writeHeader(self, scene, file):
        #.xps file needs the parent directory of the directory in which the images are saved
        parent_dir = (sep).join(realpath(scene.monkeyjam.export_path).split(sep)[:-1])
        
        extension = 'png'   #Blender's default
        if scene.render.file_extension == '.jpg':
            extension = 'jpg'
        if scene.render.file_extension == '.bmp':
            extension = 'bmp'
        
        text = '<xsheet version="4.0">\n'
        text += '<settings>\n'
        text += '\t<fps>24</fps>\n\t<step>1</step>\n\t<numbering>frame</numbering>\n'
        text += '\t<color>0</color>\n'
        text += '\t<format>{}</format>\n'.format(extension)
        text += '\t<path>{}</path>\n'.format(parent_dir)
        text += '</settings>\n'
        file.write(text)
        
        audio_path = get_audio(self, scene)
        if audio_path is not None:
            text = '<audio>\n'
            text += '\t<audiofile>{}</audiofile>\n'.format(audio_path)
            text += '\t<phonemes>\n\t</phonemes>\n'
            text += '</audio>\n'
            file.write(text)
        else:
            file.write('<audio>\n</audio>\n')
        
        text = '<layers cols="1">\n'
        text += '\t<layer col="1" caption="Layer 1" dir="{}\\" color="0" visible="1">\n'.format(realpath(scene.monkeyjam.export_path))
        file.write(text)

    
    def _writeImages(self, scene, file, image_list):
        '''A function for writing the image names to the file'''
        file_ext = scene.render.file_extension

        for i in range(len(image_list)):
            if i != len(image_list) - 1:
                duration = image_list[i+1][1] - image_list[i][1]    
            else:
                duration = 8
                
            file.write('\t<cel col="1" row="{0}" duration="{1}">{2}{3}</cel>\n'.format(image_list[i][1], duration, image_list[i][0], file_ext))
                
        
    def _writeClose(self, file):
        file.write('\t</layer>\n\t</layers>\n</xsheet>')
        
    def _closeFile(self, file):
        file.close()

    def _MJ_launch(self, mj_args, mj_cwd):
        mj_proc = Popen(mj_args, cwd = mj_cwd, bufsize = -1, shell = False)  
    
    def execute(self, context):
        active_object = context.active_object
        scene = context.scene
        output_path = realpath(scene.monkeyjam.export_path)
      
        mj_path = os.path.join(realpath(context.user_preferences.addons['blender_jam'].preferences['mj_exe_path']), 'MonkeyJam.exe')
               
        mj_filename = scene.name + '.xps'
        
        try:
            for item in os.listdir(scene.monkeyjam.export_path):
                if item[-4:] == scene.render.file_extension or '.xps':
                    os.remove('{0}\\{1}'.format(scene.monkeyjam.export_path, item))
        except:
            pass
        
        #Get the keyframes to render
        render_frames = remember_frames(context)
        self._create_image_names(scene, render_frames)
        
        mj_file = open(os.path.join(output_path, mj_filename), 'w+')
        
        self._writeHeader(scene, mj_file)
        self._writeImages(scene, mj_file, imageNames)
        self._writeClose(mj_file)
        self._closeFile(mj_file)
        print("\nWriting keyframes to .xps file...")
        
        # Start MonkeyJam with the .xps file we created:
        try:
            mj_args = [mj_path, os.path.join(output_path, mj_filename)]
            self._MJ_launch(mj_args, output_path)
            print("\nOpening MonkeyJam...")
        except:
            self.report({'INFO'}, "Cannot open MonkeyJam. Please check the MonkeyJam path in User Preferences > Addons > BlenderJam!")
            return {'CANCELLED'}

        return {'FINISHED'}

    # User will then save the .xps file in MonkeyJam

#------------------------------------------------------------------------------
#----------------------------------#
#                                  #
#   A class for reading the file   #
#                                  #
#----------------------------------#         

class PROPERTIES_OT_Read_xps(bpy.types.Operator):
    """Read new keyframes from MonkeyJam .xps file!"""
    bl_idname = "mj.read_xps"
    bl_label = "MonkeyJam Read"

    def execute(self, context):
        active_object = context.object
        scene = context.scene

        print("\n\nTrying to read frames from .xps file...")

        if not os.path.exists(realpath(scene.monkeyjam.import_file)):
            self.report( {'INFO'}, "Whoa there, turbo!  The .xps file doesn't exist!")
            return {'CANCELLED'}
        else:
            #try this a different way
            #try iterating through each fcurve and seeing if the keyframe_points.co.x
            #(ie, the location of the keyframes on the timeline) is in the original list of 
            #keyframes
            #then, if it is, push it by its corresponding offset
            #you'll have to use a pair of values, like a dict or tuple for this
            #also, don't rely on imageNames any more. that requires you to have to export FIRST.
            #we want to be able to read from the file without having to export first.
            #Get a list of the original keyframes
            row_orig = remember_frames(context)
            row_orig.reverse()
            
            mj_file = open(realpath(scene.monkeyjam.import_file), 'r')
            mj_file.seek(0)
           
            mj_lines = mj_file.readlines()
            
            #return only the complete list of images in the .xps file
            if not scene.monkeyjam.export_audio:
                mj_lines = [line.rstrip() for line in mj_lines[13:-3]]
            else:
                mj_lines = [line.rstrip() for line in mj_lines[16:-3]]
                
            mj_lines.reverse()
            
            mj_file.close() #Make sure we close the file
            
            row_new = [line.split('"')[3] for line in mj_lines]
            row_new = [int(frame) for frame in row_new]
            
            print('Old keyframes: ', row_orig[::-1])
            print('New keyframes: ', row_new[::-1])
            
            #Create a dictionary of old / new keyframe pairs    
            frame_diffs = dict(zip(row_orig, row_new))
            frame_offset = row_orig[-1] - 1   #Need the first frame of the current action, to figure out how much to offset new frames, which begin on "frame 1"
            if frame_offset:
                for key in frame_diffs:
                    frame_diffs[key] += frame_offset 
            
            print("\nAttempting to move keyframes in Blender...")
            
            fcurves = context.active_object.animation_data.action.fcurves
            
            # Now iterate through the keyframes backwards (starting at end, that is)
            # and if the current keyframe's co.x is in frame_diffs.keys(), then
            # set its co.x to the value of frame_diffs[co.x]
            for fcu in fcurves:
                for kp in reversed(fcu.keyframe_points):
                    if int(kp.co.x) in frame_diffs.keys():
                        handle_offset = frame_diffs[kp.co.x] - kp.co.x
                        kp.co.x = float(frame_diffs[kp.co.x])
                        kp.handle_left[0] += handle_offset
                        kp.handle_right[0] += handle_offset
            
            self.report( {'INFO'}, "Success, bro-ski! Moved keyframes!")
            return {'FINISHED'}
            
                  
#------------------------------------------------------------        
#------------------------------#
#                              #
#   A class for the UI items   #
#                              #
#------------------------------#

class PROPERTIES_PT_BlenderJam_Panel(bpy.types.Panel):
    bl_label = "BlenderJam!"
    bl_idname = "OBJECT_PT_blenderjam"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        layout = self.layout
        obj = context.object
        scene = context.scene
        scene_mj = scene.monkeyjam
        
        row = layout.row()
        row.scale_y = 2.0
        row.operator("mj.export_frames", text="Go MonkeyJam!", icon="MONKEY")
        
        box = layout.box()
        split = box.split(percentage = 0.40)
        col = split.column()
        col.label("Render Resolution!")
        col = split.column()
        col.prop(scene_mj, "render_globals", text = "")
        box.prop(scene_mj, "include_rigs")
        box.prop(scene_mj, "export_audio")
        if scene.monkeyjam.export_audio:
            split = box.split(percentage = 0.40)
            col = split.column()
            col.label("Audio File to Export!")
            col = split.column()
            col.prop_search(scene_mj, "audio_sequ", bpy.data, "sounds")
        
        
        split = box.split(percentage = 0.25)
        col = split.column()
        col.label("Output Path!")
        col = split.column()
        col.prop(scene_mj, "export_path", text = "")
        
        layout.separator()
        
        row = layout.row()
        row.scale_y = 1.75
        row.operator("mj.read_xps", text="Apply Timing!", icon="BLENDER")
        
        layout.separator()
        
        split = layout.split(percentage = 0.25)
        col = split.column()
        col.label("Input File!")
        col = split.column()
        col.prop(scene_mj, "import_file", text = "")

#------------------------------------------------------------
#Panel in preferences for storing MJ exe path
class PREFERENCES_PT_MJPreferencesPanel(bpy.types.AddonPreferences):
    bl_idname = __name__
    
    mj_exe_path = bpy.props.StringProperty(name = "MonkeyJam Path", description = "Path to MonkeyJam installation directory", subtype = 'DIR_PATH', default = "")

    def draw(self, context):
        self.layout.prop( self, "mj_exe_path")

#-------------------------------------------------------------
class MJ_Properties(bpy.types.PropertyGroup):
    export_path = bpy.props.StringProperty(name = "Output Path!", 
                                              description = "The path to the export directory! Don't put a filename on the end of this, seriously! I will punch you!", 
                                              subtype = 'DIR_PATH', 
                                              default = "")

    import_file = bpy.props.StringProperty(name = "Input file!", 
                                              description = "The .xps file from which to read new keyframes!", 
                                              subtype = 'FILE_PATH')

    export_audio = bpy.props.BoolProperty(name = "Export Audio!", 
                                              description = "Export audio from video sequence editor!", 
                                              default = False)

    render_globals = bpy.props.EnumProperty(items = [
                                              ('25', '25%', 'Render at 25% scene render resolution'),
                                              ('50', '50%', 'Render at 50% scene render resolution'),
                                              ('75', '75%', 'Render at 75% scene render resolution'),
                                              ('100', 'Full', 'Render at full scene render resolution')],
                                              name = "Render Resolution", 
                                              description = "Render resolution for OpenGL rendered images!",
                                              default = '50')

    audio_sequ = bpy.props.StringProperty(name = "", 
                                              description = "Audio file to export!", 
                                              default = "")

    include_rigs = bpy.props.BoolProperty( name = "Include Rigs", 
                                              description = "Include rigs in the render", 
                                              default = False)
    
    
#-------------------------------------------------------------
#Addon entry point
def register():
    bpy.utils.register_class(PROPERTIES_OT_Read_xps)
    bpy.utils.register_class(PROPERTIES_OT_Export)
    bpy.utils.register_class(PREFERENCES_PT_MJPreferencesPanel)
    bpy.utils.register_class(MJ_Properties)
    bpy.types.Scene.monkeyjam = bpy.props.PointerProperty( type = MJ_Properties)
    bpy.utils.register_class(PROPERTIES_PT_BlenderJam_Panel)
    
def unregister():
    bpy.utils.unregister_class(PROPERTIES_PT_BlenderJam_Panel)
    bpy.utils.unregister_class(PROPERTIES_OT_Read_xps)
    bpy.utils.unregister_class(PROPERTIES_OT_Export)
    bpy.utils.unregister_class(MJ_Properties)
    bpy.utils.unregister_class(PREFERENCES_PT_MJPreferencesPanel)
    del bpy.types.Scene.monkeyjam
    

if __name__ == "__main__":
    register()

                
