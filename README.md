blender_jam is a Blender addon for shooting a sequence of OpenGL viewport renders of an animated object to MonkeyJam, to facilitate quick keyframe shuffling with accurate playback speeds. The timing changes made in MonkeyJam can then be applied to the keyframes on the object in Blender.  

A brief overview:  
-The BlenderJam! panel appears in the Properties > Scene tab  
-An object with animation data (ie., an action with keyframes) should be selected  
-'Go MonkeyJam!' - exports a MonkeyJam .xps file and renders OpenGL viewport images of frames on which the object has keys  
-Render resolution - scales rendered image sizes to the selected percentage of scene render dimensions  
-Export audio - if checked, will allow you to select an audio file to export, if any exist in the video sequencer   
-Output path - the directory in which images and files should be saved. Don't put a filename on the end of this -- it's a directory path.  

-'Apply Timing!' -  applies the timing changes made in MonkeyJam to the selected object  
-Input file - the .xps file from which to read the new keyframes  

Notes:  
-Bear in mind you'll have to have the same object selected when you import as when you export, or nothing'll happen. Or maybe something'll happen, but it won't be what you want.  
-The path to the MonkeyJam.exe file is set in the user preferences, when you enable the addon.  

Please report issues to the bug tracker at:  
https://bitbucket.org/ohsnapitsjoel/blender_jam/issues  


